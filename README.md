### sGapps for Android 9 (arm64)
Customized version of SimpleGapps.  
_SimpleGapps can be found here. [Link](https://gitlab.com/ezio84/android-gapps)_  

### Changes
Replaced Pixel Setup Wizard with Google Setup Wizard  
Updated some apps  
Small includes Google Keyboard  

**Small** - Core Google and keyboard  
_Replaces keyboard with Google keyboard_  

**Mini** - Core Google  
_Doesn't remove custom rom built-in stuff_  

### Requirements to build
- Linux system with an Android build system setup	
- OpenJDK 8

### How to build
Run _git clone https://gitlab.com/ipdev99/sGapps-9.git_  
Run _mksmall.sh_ or _mkmini.sh_ to build  
Flashable zip file will be in the out directory  

### Note  
May have install issues on A/B devices running newer TWRP.

### Download Links
- _Android File Host_. [Link](https://www.androidfilehost.com/?w=files&flid=303912)
- _Google Drive_. [Link](https://drive.google.com/open?id=1RW6QfEZEgTIZMVwmK-HCdEOLjeugLLoY)
- _MediaFire_. [Link](http://www.mediafire.com/folder/l9npi56b45wv9/SDK28)

#  
  
## From SimpleGApps Read Me
**Based on Banks GApps**  

**NOTES:**
* Feel free to use, change, improve, adapt these gapps. Just remember to share them!

**CREDITS:**
* Opengapps, Banks, Beans, Surge, Nathan, Ezio, Josh, Edwin, Alex, and the Android Community
